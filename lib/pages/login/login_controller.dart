import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:locatick_flutter_app/routes/app_routes.dart';
import 'package:locatick_flutter_app/store/dbservice/db_service.dart';
import 'package:locatick_flutter_app/store/providers/_base_provider.dart';
import 'package:locatick_flutter_app/store/providers/auth_provider.dart';
import 'package:locatick_flutter_app/models/login_model.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
///
/// Piotr Zakrzewski 20.02.2021
///


enum PhoneRequestState { unknown, find, notFind }

class LoginController extends GetxController {

  AuthProvider authProvider = Get.find<AuthProvider>();
  Rx<PhoneRequestState> phoneFind = PhoneRequestState.unknown.obs;
  Rx<PhoneRequestState> codeFind = PhoneRequestState.unknown.obs;

  Worker _ever;
  Worker _ever2;


  String get phone => form.control('prefix').value + form.control('phoneNo').value;
  String get userName => form.control('userName').value;
  String get code => formSecondStep.control('security_code').value;


  @override
  void onInit() {
    super.onInit();
    _ever = ever(phoneFind, (_) => phoneRequestStatusChange());
    _ever2 = ever(codeFind, (_) => codeRequestStatusChange());

  }

  disposeWorker() {
    _ever.dispose();
    _ever2.dispose();
  }


  final form = FormGroup({
    'phoneNo':
    FormControl<String>(value: '', validators: [Validators.required]),
    'prefix':
    FormControl<String>(value: '+48', validators: [Validators.required]),
    'userName':FormControl<String>(value: '', validators: [Validators.required]),
  });

  final formSecondStep = FormGroup({
    'security_code':FormControl<String>(validators: [Validators.required])
  });

  submitForm() async {
    // print(form.value);


    var model = LoginModel(phone: phone,username: userName);
    try{
      phoneFind.value = PhoneRequestState.unknown;
      await authProvider.loginUser(model);
      phoneFind.value = PhoneRequestState.find;
    }
    catch (_){
      phoneFind.value = PhoneRequestState.notFind;
    }
  }


  submitFormSecondStep() async {
    // print(form.value);
    var model = LoginModel(phone:phone,code: code);
    try{
      codeFind.value = PhoneRequestState.unknown;
      await authProvider.sendSecurityCode(model);
      codeFind.value = PhoneRequestState.find;
      var box = Hive.box(DbService.HIVE_BOX_NAME);
      box.put(BaseProvider.CODE_KEY,model.code);
      box.put(BaseProvider.PHONE_KEY,model.phone);
    }
    catch (_){
      codeFind.value = PhoneRequestState.notFind;
    }
  }


  phoneRequestStatusChange() {
    if(phoneFind.value == PhoneRequestState.find){
      Get.toNamed(AppRoutes.LOGIN_STEP_TWO);
      Get.snackbar(
          AppLocalizations.of(Get.context).ok,
          AppLocalizations.of(Get.context).phoneFind,
          snackPosition: SnackPosition.BOTTOM
      );
    }
  }

  codeRequestStatusChange() {
    if(codeFind.value == PhoneRequestState.find){
      Get.toNamed(AppRoutes.HOME);
      Get.snackbar(
          "OK",
          "Kod poprawny",
          snackPosition: SnackPosition.BOTTOM
      );
    }
  }
}