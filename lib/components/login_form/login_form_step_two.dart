import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:locatick_flutter_app/pages/login/login_controller.dart';
import 'package:locatick_flutter_app/routes/app_routes.dart';
import 'package:reactive_forms/reactive_forms.dart';

///
/// Author zakrz 21.02.2021
///    

class LoginFormStepTwo extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
        formGroup: controller.formSecondStep,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  'Logowanie krok drugi',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Image.asset(
                'images/logo_solo.png',
                height: 100,
              ),
              SizedBox(
                width: 200,
                child: ReactiveTextField(
                  formControlName: 'security_code',
                  decoration: InputDecoration(
                    labelText: 'Kod autoryzacyjny',
                  ),
                  keyboardType: TextInputType.number,
                ),
              ),
              ReactiveFormConsumer(
                builder: (context, form, child) {
                  return ButtonBar(
                    alignment:  MainAxisAlignment.center,
                    children: [
                      OutlinedButton(child:  Text('wstecz'),onPressed:()=>Get.toNamed(AppRoutes.LOGIN)),
                      ElevatedButton(child:  Text('dalej'), onPressed: form.valid ? _submitCode:null)
                    ],
                  );
                },
              ),

            ],
          ),
        ),
    );
  }

  void _submitCode() {
    controller.submitFormSecondStep();
  }
}
