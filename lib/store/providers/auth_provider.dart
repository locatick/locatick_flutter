import 'package:dio/dio.dart';
import 'package:locatick_flutter_app/models/login_model.dart';
import 'package:sms_autofill/sms_autofill.dart';
import '_base_provider.dart';

class AuthProvider extends BaseProvider {

  Future<Response> authUser() => requestGet(url: '/auth-device');

  Future<Response> loginUser(LoginModel model) async {
    String signature = await SmsAutoFill().getAppSignature;
    print(signature);
    Map<String, String> data = {
      'phone': model.phone,
      'username': model.username,
      'hash': signature,
    };
    return await requestPost(url: '/request-code', data: data);
  }

  sendSecurityCode(LoginModel model) async {
    Map<String, String> data = {
      "code": model.code,
      "phone": model.phone
    };

    return await requestPost(url: '/request-code-confirm', data: data);
  }
}
