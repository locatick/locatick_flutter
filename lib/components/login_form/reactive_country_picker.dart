import 'package:flutter/foundation.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'flags_picker.dart';

///
/// Author Piotr Zakrzewski 20.02.2021
///
class ReactiveCountryPicker extends  ReactiveFormField<String, String>
{
     ReactiveCountryPicker({
      @required String formControlName
     }): super(
         formControlName: formControlName,
         builder: (ReactiveFormFieldState<String, String> field) {
          return FlagsPicker(
           value: field.value,
           onCountrySelected: (code) => field.didChange(code.dialCode),
          );
         }
     );

     @override
     ReactiveFormFieldState<String, String> createState() => ReactiveFormFieldState<String, String>();
}