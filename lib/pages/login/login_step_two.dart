import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:locatick_flutter_app/pages/login/login_controller.dart';
import 'package:locatick_flutter_app/components/login_form/login_form_step_two.dart';

///
/// Author zakrz 21.02.2021
///
class LoginStepTwo extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Flex(
          direction: Axis.vertical,
          children: [
            LoginFormStepTwo(),
            Obx(() =>
                Visibility(
                    child: Text('Błędny kod sms'),
                    visible: controller.codeFind.value == PhoneRequestState.notFind)),
          ]
      ),
    );
  }
}
