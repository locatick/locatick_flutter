import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

///
/// Author Piotr Zakrzewski 20.02.2021
///
@immutable
class LoginModel  extends Equatable{

  final String phone;
  final String code;
  final String username;

  LoginModel({this.phone, this.code,this.username});

  @override
  List<Object> get props => [
    phone,username,code
  ];
}