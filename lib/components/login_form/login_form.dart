import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:locatick_flutter_app/components/login_form/reactive_country_picker.dart';
import 'package:locatick_flutter_app/pages/login/login_controller.dart';
import 'package:reactive_forms/reactive_forms.dart';

///
/// Author Piotr Zakrzewski 20.02.2021
///
class LoginForm extends GetView<LoginController> {

  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
        formGroup: controller.form,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Text(
                  'Logowanie do aplikacji locatick',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Image.asset(
                'images/logo_solo.png',
                height: 100,
              ),
              Flex(
                direction: Axis.horizontal,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 20.0, 0, 0),
                      child: ReactiveCountryPicker(
                        formControlName: 'prefix',
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 250,
                    child: ReactiveTextField(
                      formControlName: 'phoneNo',
                      decoration: InputDecoration(
                        labelText: 'Nr telefonu',
                      ),
                      keyboardType: TextInputType.number,
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: 340,
                child: ReactiveTextField(
                  formControlName: 'userName',
                  decoration: InputDecoration(
                    labelText: 'Email/login',
                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              ReactiveFormConsumer(
                builder: (context, form, child) {
                  return ElevatedButton(
                    child: Text('Submit'),
                    onPressed: form.valid ? _onSubmit : null,
                  );
                },
              ),
            ],
          ),
        ));
  }

  void _onSubmit() {
    controller.submitForm();
  }
}
