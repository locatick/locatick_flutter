import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'auth_controller.dart';

///
/// Piotr Zakrzewski 20.02.2021
///
class AuthPage extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              Obx(() => Visibility(
                  child: Text(AppLocalizations.of(context).networkError),
                  visible: controller.isNetworkError.value)),
              Padding(
                padding: EdgeInsets.all(50),
                child: Obx(() => Visibility(
                    child: Text(AppLocalizations.of(context).loginToApp),
                    visible:
                    controller.isAuth.value == AuthenticationStatus.unknown)),
              )
            ],
          )
        ],
      ),
    );
  }
}