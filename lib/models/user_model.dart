import 'package:locatick_flutter_app/models/userdetail_model.dart';

///
/// Author Piotr Zakrzewski 06.04.2021
///    


class User {

  String id;
  String email;
  String roles;
  String status;
  String createdAt;
  String updatedAt;
  String fullName;
  UserDetail userDetail;

}