import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/adapter.dart';
import 'package:dio/dio.dart' as DioLib;

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:locatick_flutter_app/store/dbservice/db_service.dart';

import '_custom_http_exceptions.dart';

abstract class BaseProvider {
  static const CODE_KEY = 'USERNAME_KEY';
  static const PHONE_KEY = 'PHONE_KEY';

  DioLib.Dio _dio;

  @override
  BaseProvider() {
    // var baseUrl  = 'https://test.api.locatick.cloud/api/mobile';

    //var baseUrl = 'http://192.168.1.110:10080/api/mobile';
    var baseUrl = 'https://192.168.1.110:8443/api/mobile';
    _dio = DioLib.Dio()
      ..options.baseUrl = baseUrl;
    // ..httpClientAdapter = Http2Adapter(
    //   ConnectionManager(
    //     idleTimeout: 10000,
    //     // Ignore bad certificate
    //     onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
    //   ),
    // );


    (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate  = (client) {
      SecurityContext sc = new SecurityContext();
      //file is the path of certificate
      // sc.setTrustedCertificates(file);
      HttpClient httpClient = new HttpClient(context: sc);
      httpClient.badCertificateCallback = (X509Certificate cert, String host, int port)=>true;
      return httpClient;
    };



    _updateCreditenials();
  }

  _updateCreditenials() async {
    var box = Hive.box(DbService.HIVE_BOX_NAME);
    var _code = box.get(CODE_KEY) ?? null;
    var _phone = box.get(PHONE_KEY) ?? null;


    _dio.interceptors.add(DioLib.InterceptorsWrapper(
      onRequest:(options, handler){
        options.headers.assign('X-AUTH-TOKEN', '$_phone;$_code');
        return handler.next(options); //continue
      },
    ));


  }

  Future<DioLib.Response> requestGet({@required String url,
    Map<String, String> data,
    Function(dynamic error) onError}) async {
    try{
      return await _dio.get(url, queryParameters: data);
    }
    on DioLib.DioError catch(error){
      if (error.response != null) {
        // print(error.response.statusCode);
        // print(error.response.data);
        // print(error.response.headers);
        switch(error.response.statusCode){
          case 401:
          case 403:
          throw UnauthorisedException(error.response..toString());
            break;
          case 422:
            throw InvalidInputException(error.response.toString());
            break;
          default:
            throw BadRequestException(error.response.toString());
        }
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        // print(error.request);
        // print(error.message);
        throw FetchDataException(error.message.toString());
      }
    }
  }

  Future<DioLib.Response> requestPost({@required String url,
    Map<String, String> data,
    Function(dynamic error) onError}) async {
    try{
      return await _dio.post(url, data: jsonEncode(data));
    }
    on DioLib.DioError catch(error){
      if (error.response != null) {
        log(error.response.statusCode.toString(),name: 'HTTP ERROR');
        //log(error.response.data,name: 'HTTP ERROR');
        //log(error.response.headers.toString(),name: 'HTTP ERROR');
        switch(error.response.statusCode){
          case 401:
          case 403:
            throw new UnauthorisedException(error.response..toString());
          case 422:
            throw new InvalidInputException(error.response.toString());
          default:
            throw new BadRequestException(error.response.toString());
        }
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        //log(error.request.toString(),name: 'HTTP ERROR');
        log(error.message,name: 'HTTP ERROR');
        throw new FetchDataException(error.message.toString());
      }
    }
    // catch (error) {
    //   throw FetchDataException('NETWORK ERROR');
    // }
  }

}
