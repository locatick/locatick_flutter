import 'package:locatick_flutter_app/models/category_model.dart';
///
/// Author Piotr Zakrzewski 06.04.2021
///
class Activity {

  String id;
  String name;
  Category category;
  String dueDate;
  String dueTime;
  String closeDate;
  String owner;
  String createdAt;
  String updatedAt;
  String pcode;
  String country;
  String city;
  String address;
  String flatNo;
  String location;
  String assignedUser;
  String description;
  String status;
  String tags;
  String addressFull;
  String activityNo;
  String activityNoFull;
  String newComment;
  String objects;
  String equipment;
  String activityCustomFields;
  String attachments;
  String eventLogs;
  String count;
  String realizationStartdate;
  String responseTime;
  String workTime;
  String commentsCounty;
  String activitySchedule;
  String activityProtocols;
  String groupName;
  String groupPosition;
  String realizationDate;
  String realizationTime;
  String duration;
  String position;

}