import 'package:flutter/material.dart';

import 'light_theme.dart';

///
/// Piotr Zakrzewski 20.02.2021
///
class AppTheme {
  static ThemeData light = lightTheme;
}