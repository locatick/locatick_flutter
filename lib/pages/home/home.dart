import 'package:flutter/material.dart';
import 'package:locatick_flutter_app/components/bottomNavBar/app_bottom_nav_bar.dart';

///
/// Author zakrz 21.02.2021
///    

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Home')
      ),
      bottomNavigationBar: AppBottomNavBar()
    );
  }
}
