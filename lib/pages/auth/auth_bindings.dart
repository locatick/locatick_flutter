import 'package:get/get.dart';
import 'package:locatick_flutter_app/pages/auth/auth_controller.dart';


///
/// Piotr Zakrzewski 20.02.2021
///
class AuthBindings extends Bindings  {
  @override
  void dependencies() {
    Get.put(AuthController());
    // Get.put();
    // Get.lazyPut<Controller>(() => Controller());
    // Get.lazyPut<Controller2>(() => Controller2());
    // Get.lazyPut<Controller3>(() => Controller3());
  }

}