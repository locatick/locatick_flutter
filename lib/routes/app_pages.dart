import 'package:get/get.dart';
import 'package:locatick_flutter_app/pages/auth/auth_bindings.dart';
import 'package:locatick_flutter_app/pages/login/login_page.dart';
import 'package:locatick_flutter_app/pages/login/login_step_two.dart';
import 'package:locatick_flutter_app/pages/home/home.dart';
import 'package:locatick_flutter_app/pages/auth/auth_page.dart';
import 'package:locatick_flutter_app/routes/app_routes.dart';

///
/// Piotr Zakrzewski 20.02.2021
///
class AppPages {
  static final list = [
      GetPage(name: AppRoutes.INITROUTE, page: ()=>AuthPage(),binding: AuthBindings()),
      GetPage(name: AppRoutes.LOGIN, page: ()=>LoginPage()),
      GetPage(name: AppRoutes.LOGIN_STEP_TWO, page: ()=>LoginStepTwo()),
      GetPage(name: AppRoutes.HOME, page: ()=>Home())
  ];
}