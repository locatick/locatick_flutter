import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:locatick_flutter_app/components/login_form/login_form.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'login_controller.dart';

///
/// Piotr Zakrzewski 20.02.2021
///
class LoginPage extends GetView<LoginController> {

  @override
  Widget build(BuildContext context) {
    Get.put(LoginController());

    return Scaffold(
        body: Flex(
          direction: Axis.vertical,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            LoginForm(),
            Obx(() =>
                Visibility(
                    child: Text(AppLocalizations.of(context).phoneNotFind),
                    visible: controller.phoneFind.value == PhoneRequestState.notFind)),
          ],
        )
    );
  }
}
