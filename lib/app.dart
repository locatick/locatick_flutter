// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:locatick_flutter_app/src/routes/routes.dart';
// import 'package:locatick_flutter_app/src/screens/init/init.dart';
// import 'package:locatick_flutter_app/src/screens/login/login.dart';
// import 'package:locatick_flutter_app/src/screens/main/main.dart';
// import 'package:locatick_flutter_app/src/shared/middleware/auth_middleware.dart';
//
// import 'src/store/auth/auth_controller.dart';
//
// class App extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//
//     Get.put(AuthController());
//
//     return GetMaterialApp(
//       enableLog: true,
//       defaultTransition: Transition.fade,
//       getPages: [
//         GetPage(name: Routes.init, page: () => InitScreen()),
//         GetPage(name: Routes.main, page: () => MainScreen(),
//             middlewares: [
//               AuthMiddleware(0),
//             ]
//         ),
//         GetPage(
//             name: Routes.login,
//             page: () => LoginScreen(),
//         ),
//
//       ],
//     );
//   }
// }
