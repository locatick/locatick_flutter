import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

///
/// Piotr Zakrzewski 20.02.2021
///
class DbService extends GetxService{

  static const HIVE_BOX_NAME = 'settings_box';


  Future<DbService> init() async {
    await Hive.initFlutter();
    await Hive.openBox(HIVE_BOX_NAME);

    print('$runtimeType delays 2 sec');
    await 2.delay();
    print('$runtimeType ready!');
    return this;
  }

  // init() async {
  //   //await Hive.();
  //   // await Hive.openBox(HIVE_BOX_NAME);
  // }


}