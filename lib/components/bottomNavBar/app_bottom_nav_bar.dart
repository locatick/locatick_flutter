import 'package:flutter/material.dart';

///
/// Author zakrz 21.02.2021
///

class AppBottomNavBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppBottomNavBarState();
}

class _AppBottomNavBarState extends State<AppBottomNavBar> {

  int _selectedIndex = 0;


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }



  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
            label: 'Aktywności',
            icon: Icon(
              Icons.home,
            )),
        BottomNavigationBarItem(
            label: 'Mapa',
            icon: Icon(
              Icons.map,
            ))
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.blue[800],
      onTap: _onItemTapped,
    );
  }
}
