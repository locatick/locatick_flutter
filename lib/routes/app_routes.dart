///
/// Piotr Zakrzewski 20.02.2021
///
class AppRoutes {
  static const String INITROUTE = '/';
  static const String LOGIN = '/login';
  static const String LOGIN_STEP_TWO = "/auth_step_two";

  static const String  HOME = '/home';
}