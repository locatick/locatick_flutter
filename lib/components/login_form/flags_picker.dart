import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';


typedef void CountrySelectedCallback(CountryCode code);

class FlagsPicker extends StatelessWidget {


  final String value;
  final CountrySelectedCallback onCountrySelected;

  const FlagsPicker({Key key, @required this.value, this.onCountrySelected}) : super(key: key);


  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 60,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: CountryCodePicker(
          onChanged: onCountrySelected,
          // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
          initialSelection: this.value,
          countryFilter: ['PL','DE','+44', 'CZ','SK',],
          // optional. Shows only country name and flag
          showCountryOnly: false,
          // optional. Shows only country name and flag when popup is closed.
          showOnlyCountryWhenClosed: false,
          dialogSize: Size.fromWidth(350),

          // optional. aligns the flag and the Text left
          alignLeft: false,
        ),
      ),
    );
  }
}
