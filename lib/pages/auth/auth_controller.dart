import 'package:get/get.dart';
import 'package:locatick_flutter_app/routes/app_routes.dart';
import 'package:locatick_flutter_app/store/providers/_custom_http_exceptions.dart';
import 'package:locatick_flutter_app/store/providers/auth_provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
///
/// Piotr Zakrzewski 20.02.2021
///

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

class AuthController extends GetxController {
  final isAuth = AuthenticationStatus.unknown.obs;
  final provider = Get.put(AuthProvider());
  final isNetworkError = false.obs;

  @override
  void onInit() async {
    super.onInit();
    ever(isAuth, (_) => authStatusChanged());

    authUser();
  }

  login() {
    isAuth.value = AuthenticationStatus.authenticated;
    isAuth.refresh();
    Get.toNamed(AppRoutes.HOME);
    Get.snackbar(
        AppLocalizations.of(Get.context).ok,
        AppLocalizations.of(Get.context).loginOK,
        snackPosition: SnackPosition.BOTTOM
    );
  }

  logout() {
    isAuth.value = AuthenticationStatus.unauthenticated;
    isAuth.refresh();
  }

  authStatusChanged() {
    if (isAuth.value == AuthenticationStatus.unauthenticated) {
      Get.toNamed(AppRoutes.LOGIN);
    }
  }

  Future<void> authUser() async {
    try {
      var response = await provider.authUser();
      if (response.statusCode == 200 || response.statusCode == 201) {
        login();
      }
    } on UnauthorisedException catch (_) {
      logout();
    } catch (_) {
      isNetworkError.value = true;
      isNetworkError.refresh();
    }
  }
}
